package br.ufrn.imd.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.ufrn.imd.app.android.R;
import br.ufrn.imd.app.dominio.Fornecedor;

/**
 * Created by itamir on 07/05/2015.
 */
public class FornecedorListaAdapter extends ArrayAdapter<Fornecedor> {

    public FornecedorListaAdapter(Context context, int
            textViewResourceId,List<Fornecedor> objects) {
        super(context, textViewResourceId, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_fornecedores, null);
        }

        Fornecedor item = getItem(position);
        if (item!= null) {
            TextView titleText = (TextView) view.findViewById(R.id.nomeText);
            titleText.setText( item.getNome() );
            TextView cnpjText = (TextView) view.findViewById(R.id.cnpjText);
            cnpjText.setText(item.getCnpj());

        }

        return view;
    }
}
